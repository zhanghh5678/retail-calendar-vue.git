import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import globalStyle from '@originjs/vite-plugin-global-style'
import { resolve } from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  resolve:{
    alias:{
      // 键必须以斜线开始和结束
      '@': resolve(__dirname, './src'), // 设置别名
      '@components':resolve(__dirname, './components') // 设置别名
    }
  },
  plugins: [
    vue(),
    globalStyle({
      sourcePath:'./src/assets/css',
      lessEnabled:true
    })
  ],
})
