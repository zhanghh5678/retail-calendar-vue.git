/**
 * 颜色
 */
import _ from 'lodash';
export const BG_COLORS = ['','blue','indigo',  'green','orange','deep-purple', 'grey'];


export const getRandomBgColor = ()=>{
  const index = _.random(0, BG_COLORS.length-1);
  // console.log('getRandomBgColor==>', [index, BG_COLORS[index]])
  return BG_COLORS[index];
}

