import dayjs from 'dayjs'

import { WEEK_DAYS, YMD } from './constants'

export const createDayjs = (dateStr, format)=>{
  return dayjs(dateStr, format);
}
/**
 * 获取当前月份日期数据
 * @param {*} date 
 * @returns 
 */
export const getMonthDate = (date)=>{
  if(!date){
    date = createDayjs();
  }
  const firstDay = date.startOf('month');
  const lastDay = date.endOf('month');

  const total = date.daysInMonth();

  const firstWeekDay = firstDay.day();
  const lastWeekDay = lastDay.day();
  
  return {
    date, 
    
    firstDay, // 本月1号
    lastDay,  // 本月最后一天
    total, // 总共多少天
    firstWeekDay, // 1号星期几
    lastWeekDay, // 本月最后一天星期几
    
    
    ymd: date.format(YMD),
    yearMonth: date.format('YYYYMM'),
    year: date.format('YYYY'),
    month: date.format('MM'),
    day: date.format('DD')
  }
}

/**
 * 获取当前月份日历数据
 */
export const getCalendarDate = (date)=>{
  const monthDate = getMonthDate(date);

  const { 
    firstWeekDay, lastWeekDay, total,
    firstDay, lastDay, yearMonth
  } = monthDate;

  const beginFirst = firstWeekDay === 0;
  const endLast = lastWeekDay === 6;

  // const rowsCount = (total-( 7 - firstWeekDay ) - (lastWeekDay+1)) / 7 + (beginFirst ? 0 : 1) + (endLast ? 0 : 1);

  const dayTotal = total + firstWeekDay + (7-lastWeekDay-1);
  const rowsCount = dayTotal/7;

  const beginDate = firstDay.clone().subtract(beginFirst ? 0: firstWeekDay,'day');
  const endDate = lastDay.clone().add(endLast ? 0: 7-lastWeekDay-1,'day');

  const records = getDateRangeList(beginDate, dayTotal, yearMonth);
  
  return {
    ...monthDate,
    rowsCount,
    beginDate: beginDate.format(YMD),
    endDate: endDate.format(YMD),
    records
  }
}

/**
 * 获取日期范围
 * @param {*} beginDate 
 * @param {*} endDate 
 */
export const getDateRangeList = (beginDate, total, yearMonth)=>{
  const records = [];
  let date = beginDate.clone();
  let count = 0;
  while(count < total){
    const isSameMonth = yearMonth  === date.format('YYYYMM');
    records.push({
      index: count,
      date: date.clone(),
      ymd: date.format(YMD),
      year: date.format('YYYY'),
      month: date.format('MM'),
      day: date.format('DD'),
      isSameMonth
    });
    date = date.add(1, 'day')
    count += 1;
  }
  return records;
}

export const getPrev = (date, unit="month") =>{ 
  return date.clone().subtract(1, unit)
}

export const getNext = (date, unit="month") =>{ 
  return date.clone().add(1, unit)
}

// console.log('====>',getCalendarDate('2023-01-30', 'YYYY-MM-DD')) 

// window.dayjs = dayjs;


export default {

}


